package com.waraporn.week11;

public class Fish extends Animal implements Swimable {
    public Fish(String name) {
        super(name, 0);
    }
    @Override
    public void eat() {
        System.out.println(this + " eat.");   
    }
    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
    }
    @Override
    public String toString() {
        return "Fish(" + this.getName()+")";
    }
    @Override
    public void swim() {
        System.out.println(this + " swim.");
    }
}
