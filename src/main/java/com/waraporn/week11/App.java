package com.waraporn.week11;

public class App 
{
    public static void main( String[] args )
    {
        Bat bat = new Bat("Vampee");
        bat.eat();
        bat.sleep();
        bat.fly();
        bat.takeoff();
        bat.landing();
        System.out.println("");

        Bird bird = new Bird("Birdee");
        bird.eat();
        bird.sleep();
        bird.walk();
        bird.fly();
        bird.takeoff();
        bird.landing();
        System.out.println("");

        Rat rat = new Rat("Raddee");
        rat.eat();
        rat.sleep();
        rat.walk();
        System.out.println("");

        Human human = new Human("Tonq");
        human.eat();
        human.sleep();
        human.walk();
        System.out.println("");

        Dog dog = new Dog("DigDig");
        dog.eat();
        dog.sleep();
        dog.walk();
        System.out.println("");

        Cat cat = new Cat("Kittee");
        cat.eat();
        cat.sleep();
        cat.walk();
        System.out.println("");

        Fish fish = new Fish("FeeFi");
        fish.eat();
        fish.sleep();
        fish.swim();
        System.out.println("");

        Crocodile croc = new Crocodile("Kackee");
        croc.eat();
        croc.sleep();
        croc.swim();
        croc.crawl();
        System.out.println("");

        Snake snake = new Snake("Sanak");
        snake.eat();
        snake.sleep();
        snake.crawl();
        System.out.println("");
         
        Plane plane = new Plane("Emirates", "A380");
        plane.fly();
        plane.takeoff();
        plane.landing();
        System.out.println("");

        Submarine smr = new Submarine("Barracude", "SS-163");
        smr.swim();
        System.out.println("");

        Walkable[] walkablesObjects = {bird,cat,dog,human,rat};
        for(int i=0; i<walkablesObjects.length; i++) {
            walkablesObjects[i].walk();
        }
        System.out.println("");
        Flyable[] flyablesObjects = {bat,bird,plane};
        for(int i=0; i<flyablesObjects.length; i++) {
            flyablesObjects[i].fly();;
            flyablesObjects[i].takeoff();
            flyablesObjects[i].landing();
        }
        System.out.println("");
        Swimable[] swimablesObjects = {croc,fish,smr};
        for(int i=0; i<swimablesObjects.length; i++) {
            swimablesObjects[i].swim();
        }
        System.out.println("");
        Crawlable[] crawlablesObjects = {croc,snake};
        for(int i=0; i<crawlablesObjects.length; i++) {
            crawlablesObjects[i].crawl();
        }
    }
}
