package com.waraporn.week11;

public interface Flyable {
    public void fly();
    public void takeoff();
    public void landing();
}
