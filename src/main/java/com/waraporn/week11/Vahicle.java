package com.waraporn.week11;

public abstract class Vahicle {
    private String name;
    private String engineName;

    public Vahicle(String name, String engineName) {
        this.name = name;
        this.engineName = engineName;
    }
    public String getName() {
        return name;
    }
    public String getEngineName() {
        return engineName;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setEngineName(String engineName) {
        this.engineName = engineName;
    }
}
